﻿using DataAccessLibrary.DataAccess_Offline;
using DataAccessLibrary.Models;
using Microsoft.Data.Sqlite;
using System;
using System.Collections.Generic;
using System.IO;

namespace DataAccessLibrary
{
    public static class DataBLL
    {
        public static void SyncDB_BLL(List<obj_Row> listobj, obj_Table obj, string dbpath)
        {
            try
            {
                string query = string.Empty;
                using (SqliteConnection db =
                   new SqliteConnection($"Filename={dbpath}"))
                {
                    db.Open();
                    using (var trans = db.BeginTransaction())
                    {

                        SqliteCommand CreateCommand = null;
                        foreach (var o in obj.table_name)
                        {
                            CreateCommand = db.CreateCommand();
                            CreateCommand.Transaction = trans;
                            CreateCommand.CommandText = "DELETE FROM " + o.Value + ";";
                            CreateCommand.ExecuteNonQuery();
                        }
                        foreach (var queryCommand in listobj)
                        {
                            CreateCommand = db.CreateCommand();
                            CreateCommand.Transaction = trans;
                            CreateCommand.CommandText = queryCommand.Value;
                            CreateCommand.ExecuteNonQuery();
                        }
                        trans.Commit();
                    }
                    db.Close();
                }
            }
            catch (Exception ex) { }
        }
    }
}
