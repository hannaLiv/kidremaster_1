﻿using DataAccessLibrary.DataAccess_Offline;
using DataAccessLibrary.DataAccess_Online;
using DataAccessLibrary.Models;
using Microsoft.Data.Sqlite;
using System;
using System.Collections.Generic;
using System.IO;

namespace DataAccessLibrary
{
    public static class DataAccess
    {
        #region Answer
        public static List<Answer> Answer_GetDataByQuestionId(bool isModeOnline, string dbpath, string url_webapi, int id)
        {
            var result = new List<Answer>();
            if (isModeOnline)
            {
                result = Answer_On.GetListAnswerByQuestionId(url_webapi, id);
            }

            return result;
        }
        public static string Answer_SaveAnswer(bool isModeOnline, string dbpath, string url_webapi, Answer ans)
        {
            string message = "";
            if (isModeOnline)
            {
                message = Answer_On.SaveAnswer(url_webapi, ans);
            }
            else
            {
                message = "";
            }
            return message;

        }
        public static string Answer_DeleteAnswer(bool isModeOnline, string dbpath, string url_webapi, int id)
        {
            string message = "";
            if (isModeOnline)
            {
                message = Answer_On.DeleteAnswer(url_webapi, id);
            }
            else
            {
                message = "";
            }
            return message;

        }
        #endregion
        #region Question
        public static List<Question> Question_GetData(bool isModeOnline, string dbpath, string url_webapi)
        {
            var result = new List<Question>();
            if (isModeOnline)
            {
                result = Question_On.GetListQuestion(url_webapi);
            }

            return result;
        }
        public static List<Question> Question_GetDataByMultiChoiceId(bool isModeOnline, string dbpath, string url_webapi, int id)
        {
            var result = new List<Question>();
            if (isModeOnline)
            {
                result = Question_On.GetListQuestionByMultiChoiceId(url_webapi, id);
            }

            return result;
        }
        public static Question Question_GetById(bool isModeOnline, string dbpath, string url_webapi, int id)
        {
            var result = new Question();
            if (isModeOnline)
                result = Question_On.GetQuestionById(url_webapi, id);
            return result;
        }
        public static string Question_SaveQuestion(bool isModeOnline, string dbpath, string url_webapi, Question question)
        {
            string message = "";
            if (isModeOnline)
            {
                message = Question_On.SaveQuestion(url_webapi, question);
            }
            else
            {
                message = "";
            }
            return message;

        }
        public static string Question_Delete(bool isModeOnline, string dbpath, string url_webapi, int id)
        {
            string message = "";
            if (isModeOnline)
            {
                message = Question_On.DeleteQuestion(url_webapi, id);
            }
            else
            {
                message = "";
            }
            return message;

        }
        #endregion
        #region MultiChoice
        public static List<MultiChoice> MultiChoice_GetData(bool isModeOnline, string dbpath, string url_webapi)
        {
            var result = new List<MultiChoice>();
            if (isModeOnline)
            {
                result = MultiChoice_On.GetListMultiChoice(url_webapi);
            }
            
            return result;
        }
        public static MultiChoice MultiChoice_GetById(bool isModeOnline, string dbpath, string url_webapi, int id)
        {
            
            var result = new MultiChoice();
            if(id > 0)
                if (isModeOnline)
                    result = MultiChoice_On.GetListMultiChoiceById(url_webapi, id);
            return result;
        }
        public static string MultiChoice_InsertData(bool isModeOnline, string dbpath, string url_webapi, MultiChoice multi)
        {
            string message = "";
            if (isModeOnline)
            {
                message = MultiChoice_On.InsertMultiChoice(url_webapi, multi);
            }
            else
            {
                message = "";
            }
            return message;
        }
        public static string MultiChoice_UpdateData(bool isModeOnline, string dbpath, string url_webapi, MultiChoice multi)
        {
            string message = "";
            if (isModeOnline)
            {
                message = MultiChoice_On.UpdateMultiChoice(url_webapi, multi);
            }
            else
            {
                message = "";
            }
            return message;
        }
        public static string MultiChoice_DeleteData(bool isModeOnline, string dbpath, string url_webapi, int id)
        {
            var result = "";
            if (id > 0)
                if (isModeOnline)
                    result = MultiChoice_On.DeleteMultiChoice(url_webapi, id);
            return result;
        }
        #endregion
        public static List<Copyright> Copyright_GetData(bool isModeOnline, string dbpath, string url_webapi)
        {
            List<Copyright> entries = new List<Copyright>();
            if (isModeOnline)
            {
                entries = Copyright_On.GetData(url_webapi);
            }   
            else
            {                
                entries = Copyright_Off.GetData(dbpath);
            }
            return entries;
        }
        public static List<Menu> Menu_GetData(bool isModeOnline, string dbpath, string url_webapi)
        {
            List<Menu> entries = new List<Menu>();
            if (isModeOnline)
            {
                entries = Menu_On.GetData(url_webapi);
            }
            else
            {
                entries = Menu_Off.GetData(dbpath);
            }
            return entries;
        }
        public static Menu Menu_objGetData(bool isModeOnline, string dbpath, string url_webapi, string id)
        {
            Menu entries = new Menu();
            if (isModeOnline)
            {
                entries = Menu_On.objGetData(url_webapi, id);
            }
            else
            {
                //entries = Menu_Off.GetData(dbpath);
            }
            return entries;
        }
        public static string Copyright_InsertData(bool isModeOnline, string dbpath, string url_webapi)
        {
            string message = "";
            if (isModeOnline)
            {
                message = Copyright_On.InsertData(url_webapi);
            }
            else
            {
                message = "";
            }
            return message;
        }
        public static string Menu_InsertData(bool isModeOnline, string dbpath, string url_webapi, Menu oMenu)
        {
            string message = "";
            if (isModeOnline)
            {
                message = Menu_On.InsertData(url_webapi, oMenu);
            }
            else
            {
                message = "";
            }
            return message;
        }

        public static string Menu_UpdateData(bool isModeOnline, string dbpath, string url_webapi, Menu oMenu)
        {
            string message = "";
            if (isModeOnline)
            {
                message = Menu_On.UpdateData(url_webapi, oMenu);
            }
            else
            {
                message = "";
            }
            return message;
        }
        public static string Menu_DeleteData(bool isModeOnline, string dbpath, string url_webapi, string Id)
        {
            string message = "";
            if (isModeOnline)
            {
                message = Menu_On.DeleteData(url_webapi, Id);
            }
            else
            {
                message = "";
            }
            return message;
        }

        public static string User_Login(bool isModeOnline, string dbpath, string url_webapi, string username, string password)
        {
            string message = "";
            if (isModeOnline)
            {
                message = User_On.Login(url_webapi, username, password);
            }
            else
            {
                message = "";
            }
            return message;
        }
    }
}
