﻿using DataAccessLibrary.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace DataAccessLibrary.DataAccess_Offline
{
    public class Copyright_On
    {
        public static List<Copyright> GetData(string url_webapi)
        {
            List<Copyright> entries = new List<Copyright>();
            try
            {
                HttpClient client = new HttpClient();
                var resp = client.GetAsync(url_webapi + "Copyright/GetData").Result;
                string result = resp.Content.ReadAsStringAsync().Result;
                if (!string.IsNullOrEmpty(result))
                    entries = JsonConvert.DeserializeObject<List<Copyright>>(result);
            }
            catch (Exception ex) { return entries = new List<Copyright>(); }
            return entries;
        }
        public static string InsertData(string url_webapi)
        {
            string message = Const.ThemMoiThanhCong;
            try
            {
                HttpClient client = new HttpClient();
                Copyright obj = new Copyright();
                obj.CompanyName = "baovu1";
                obj.YearOfCopy = 2020;
                obj.LicenseType = "1.0";
                obj.Address = "Hà Nội";
                obj.Telephone = "0375931846";
                obj.Email = "vuhuubao22@gmail.com";
                string jsonObj = JsonConvert.SerializeObject(obj);
                var content = new StringContent(jsonObj, System.Text.Encoding.UTF8, "application/json");
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                var resp = client.PostAsync(url_webapi + "Copyright/InsertData", content).Result;
                string result = resp.Content.ReadAsStringAsync().Result;
            }
            catch (Exception ex) { return ex.Message; }
            return message;
        }
    }
}
