﻿using DataAccessLibrary.Models;
using DataAccessLibrary.Untility;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace DataAccessLibrary.DataAccess_Online
{
    public class MultiChoice_On
    {
        public static List<MultiChoice> GetListMultiChoice(string url_webapi)
        {
            var result = new List<MultiChoice>();
            try
            {
                HttpClient client = new HttpClient();
                //var jsonObj = JsonConvert.SerializeObject(filter);
                //var content = new StringContent(jsonObj, System.Text.Encoding.UTF8, "application/json");
                //content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                var resp = client.GetAsync(url_webapi + "api/MultiChoices").Result;
                string r = resp.Content.ReadAsStringAsync().Result;
                if (!string.IsNullOrEmpty(r))
                    result = JsonConvert.DeserializeObject<List<MultiChoice>>(r);
            }
            catch (Exception ex) { return result = new List<MultiChoice>(); }
            return result;
        }
        public static MultiChoice GetListMultiChoiceById(string url_webapi, int id)
        {
            var result = new MultiChoice();
            try
            {
                HttpClient client = new HttpClient();
                //var jsonObj = JsonConvert.SerializeObject(filter);
                //var content = new StringContent(jsonObj, System.Text.Encoding.UTF8, "application/json");
                //content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                var resp = client.GetAsync(url_webapi + "api/MultiChoices/" + id).Result;
                string r = resp.Content.ReadAsStringAsync().Result;
                if (!string.IsNullOrEmpty(r))
                    result = JsonConvert.DeserializeObject<MultiChoice>(r);
            }
            catch (Exception ex) { return result = new MultiChoice(); }
            return result;
        }
        public static string InsertMultiChoice(string url_webapi, MultiChoice multi)
        {
            try
            {
                HttpClient client = new HttpClient();
                var jsonObj = JsonConvert.SerializeObject(multi);
                var content = new StringContent(jsonObj, System.Text.Encoding.UTF8, "application/json");
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                var resp = client.PostAsync(url_webapi + "api/MultiChoices", content).Result;
                string r = resp.Content.ReadAsStringAsync().Result;
                //MultiChoicesController
                return r;
            }
            catch (Exception ex) { return ex.Message; }
        }
        public static string UpdateMultiChoice(string url_webapi, MultiChoice multi)
        {
            try
            {
                HttpClient client = new HttpClient();
                var jsonObj = JsonConvert.SerializeObject(multi);
                var content = new StringContent(jsonObj, System.Text.Encoding.UTF8, "application/json");
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                var resp = client.PutAsync(url_webapi + "api/MultiChoices/" + multi.Id, content).Result;
                string r = resp.Content.ReadAsStringAsync().Result;
                return r;
            }
            catch (Exception ex) { return ex.Message; }
        }
        public static string DeleteMultiChoice(string url_webapi, int id)
        {
            if(id > 0)
            {
                try
                {
                    HttpClient client = new HttpClient();
                    var resp = client.DeleteAsync(url_webapi + "api/MultiChoices/" + id).Result;
                    string r = resp.Content.ReadAsStringAsync().Result;
                    return r;
                }
                catch (Exception ex) { return ex.Message; }
            }
            return "Error! Id must more than zero";
        }
    }
}
