﻿using DataAccessLibrary.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace DataAccessLibrary.DataAccess_Online
{
    public class Menu_On
    {
        public static List<Menu> GetData(string url_webapi)
        {
            List<Menu> entries = new List<Menu>();
            try
            {
                HttpClient client = new HttpClient();
                var resp = client.GetAsync(url_webapi + "Menu/GetData").Result;
                string result = resp.Content.ReadAsStringAsync().Result;
                if (!string.IsNullOrEmpty(result))
                    entries = JsonConvert.DeserializeObject<List<Menu>>(result);
            }
            catch (Exception ex) { return entries = new List<Menu>(); }
            return entries;
        }
        public static Menu objGetData(string url_webapi, string id)
        {
            Menu entries = new Menu();
            try
            {
                HttpClient client = new HttpClient();
                var resp = client.GetAsync(url_webapi + "Menu/objGetData?id=" + id).Result;
                string result = resp.Content.ReadAsStringAsync().Result;
                if (!string.IsNullOrEmpty(result))
                    entries = JsonConvert.DeserializeObject<Menu>(result);
            }
            catch (Exception ex) { return entries = new Menu(); }
            return entries;
        }
        public static string InsertData(string url_webapi, Menu oMenu)
        {
            string message = Const.ThemMoiThanhCong;
            try
            {
                HttpClient client = new HttpClient();
                string jsonObj = JsonConvert.SerializeObject(oMenu);
                var content = new StringContent(jsonObj, System.Text.Encoding.UTF8, "application/json");
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                var resp = client.PostAsync(url_webapi + "Menu/InsertData", content).Result;
                string result = resp.Content.ReadAsStringAsync().Result;
            }
            catch (Exception ex) { return ex.Message; }
            return message;
        }
        public static string UpdateData(string url_webapi, Menu oMenu)
        {
            string message = Const.CapNhatThanhCong;
            try
            {
                HttpClient client = new HttpClient();
                string jsonObj = JsonConvert.SerializeObject(oMenu);
                var content = new StringContent(jsonObj, System.Text.Encoding.UTF8, "application/json");
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                var resp = client.PostAsync(url_webapi + "Menu/UpdateData", content).Result;
                string result = resp.Content.ReadAsStringAsync().Result;
            }
            catch (Exception ex) { return ex.Message; }
            return message;
        }
        public static string DeleteData(string url_webapi, string id)
        {
            string message = Const.XoaThanhCong;
            try
            {
                HttpClient client = new HttpClient();
                var resp = client.GetAsync(url_webapi + "Menu/DeleteData?id=" + id).Result;
                string result = resp.Content.ReadAsStringAsync().Result;
            }
            catch (Exception ex) { return ex.Message; }
            return message;
        }
    }
}
