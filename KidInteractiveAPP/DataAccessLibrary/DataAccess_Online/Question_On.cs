﻿using DataAccessLibrary.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace DataAccessLibrary.DataAccess_Online
{
    public class Question_On
    {
        public static List<Question> GetListQuestion(string url_webapi)
        {
            var result = new List<Question>();
            try
            {
                HttpClient client = new HttpClient();
                //var jsonObj = JsonConvert.SerializeObject(filter);
                //var content = new StringContent(jsonObj, System.Text.Encoding.UTF8, "application/json");
                //content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                var resp = client.GetAsync(url_webapi + "api/Questions").Result;
                string r = resp.Content.ReadAsStringAsync().Result;
                if (!string.IsNullOrEmpty(r))
                    result = JsonConvert.DeserializeObject<List<Question>>(r);
            }
            catch (Exception ex) { return result = new List<Question>(); }
            return result;
        }
        public static List<Question> GetListQuestionByMultiChoiceId(string url_webapi, int id)
        {
            var result = new List<Question>();
            try
            {
                HttpClient client = new HttpClient();
                //var jsonObj = JsonConvert.SerializeObject(filter);
                //var content = new StringContent(jsonObj, System.Text.Encoding.UTF8, "application/json");
                //content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                var resp = client.GetAsync(url_webapi + "api/Questions/MultiChoice/" + id).Result;
                string r = resp.Content.ReadAsStringAsync().Result;
                if (!string.IsNullOrEmpty(r))
                    result = JsonConvert.DeserializeObject<List<Question>>(r);
            }
            catch (Exception ex) { return result = new List<Question>(); }
            return result;
        }
        public static Question GetQuestionById(string url_webapi, int id)
        {
            var result = new Question();
            if(id > 0)
            {
                try
                {
                    HttpClient client = new HttpClient();
                    //var jsonObj = JsonConvert.SerializeObject(filter);
                    //var content = new StringContent(jsonObj, System.Text.Encoding.UTF8, "application/json");
                    //content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    var resp = client.GetAsync(url_webapi + "api/Questions/"+id).Result;
                    string r = resp.Content.ReadAsStringAsync().Result;
                    if (!string.IsNullOrEmpty(r))
                        result = JsonConvert.DeserializeObject<Question>(r);
                }
                catch (Exception ex) { return result = new Question(); }
            }
            
            return result;
        }
        public static string DeleteQuestion(string url_webapi, int id)
        {
            
            if (id > 0)
            {
                try
                {
                    HttpClient client = new HttpClient();
                    //var jsonObj = JsonConvert.SerializeObject(filter);
                    //var content = new StringContent(jsonObj, System.Text.Encoding.UTF8, "application/json");
                    //content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    var resp = client.DeleteAsync(url_webapi + "api/Questions/" + id).Result;
                    string r = resp.Content.ReadAsStringAsync().Result;
                }
                catch (Exception ex) { return ex.Message; }
            }

            return "Id must higher than zero";
        }

        public static string SaveQuestion(string url_webapi, Question question)
        {
            try
            {
                HttpClient client = new HttpClient();
                var jsonObj = JsonConvert.SerializeObject(question);
                var content = new StringContent(jsonObj, System.Text.Encoding.UTF8, "application/json");
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                if(question.Id == 0)
                {
                    var resp = client.PostAsync(url_webapi + "api/Questions", content).Result;
                    string r = resp.Content.ReadAsStringAsync().Result;
                    //MultiChoicesController
                    return r;
                }
                else if(question.Id > 0)
                {
                    var resp = client.PutAsync(url_webapi + "api/Questions/" + question.Id, content).Result;
                    string r = resp.Content.ReadAsStringAsync().Result;
                    //MultiChoicesController
                    return r;
                }
                else
                {
                    return "Error";
                }
                
            }
            catch (Exception ex) { return ex.Message; }
        }
    }
}
