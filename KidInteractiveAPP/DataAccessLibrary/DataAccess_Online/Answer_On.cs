﻿using DataAccessLibrary.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace DataAccessLibrary.DataAccess_Online
{
    public class Answer_On
    {
        public static List<Answer> GetListAnswerByQuestionId(string url_webapi, int id)
        {
            var result = new List<Answer>();
            try
            {
                HttpClient client = new HttpClient();
                //var jsonObj = JsonConvert.SerializeObject(filter);
                //var content = new StringContent(jsonObj, System.Text.Encoding.UTF8, "application/json");
                //content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                var resp = client.GetAsync(url_webapi + "api/Answers/GetAnswersByQuestionId/"+id).Result;
                string r = resp.Content.ReadAsStringAsync().Result;
                if (!string.IsNullOrEmpty(r))
                    result = JsonConvert.DeserializeObject<List<Answer>>(r);
            }
            catch (Exception ex) { return result = new List<Answer>(); }
            return result;
        }
        public static string SaveAnswer(string url_webapi, Answer ans)
        {
            try
            {
                HttpClient client = new HttpClient();
                var jsonObj = JsonConvert.SerializeObject(ans);
                var content = new StringContent(jsonObj, System.Text.Encoding.UTF8, "application/json");
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                if (ans.Id == 0)
                {
                    var resp = client.PostAsync(url_webapi + "api/Answers", content).Result;
                    string r = resp.Content.ReadAsStringAsync().Result;
                    //MultiChoicesController
                    return r;
                }
                else if (ans.Id > 0)
                {
                    var resp = client.PutAsync(url_webapi + "api/Answers/" + ans.Id, content).Result;
                    string r = resp.Content.ReadAsStringAsync().Result;
                    //MultiChoicesController
                    return r;
                }
                else
                {
                    return "Error";
                }
            }
            catch (Exception ex) { return ex.Message; }
        }
        public static string DeleteAnswer(string url_webapi, int id)
        {
            try
            {
                HttpClient client = new HttpClient();
                if (id > 0)
                {
                    var resp = client.DeleteAsync(url_webapi + "api/Answers/" + id).Result;
                    string r = resp.Content.ReadAsStringAsync().Result;
                    //MultiChoicesController
                    return r;
                }
                return "Error! Id must higher than zero";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}
