﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLibrary
{
    public class Const
    {
        public static string ThemMoiThanhCong = "Thêm mới thành công !";
        public static string CapNhatThanhCong = "Cập nhật thành công !";
        public static string XoaThanhCong = "Xóa thành công !";
        public static string ThemMoiThatBai = "Thêm mới thất bại !";
        public static string CapNhatThatBai = "Cập nhật thất bại !";
        public static string XoaThatBai = "Xóa thất bại !";
        public static string DangNhapThanhCong = "Đăng nhập thành công !";
        public static string DangNhapThatBai = "Đăng nhập thất bại !";
        public static string DangKyThanhCong = "Đăng ký thành công !";
        public static string DangKyThatBai = "Đăng ký thất bại !";
    }
}
