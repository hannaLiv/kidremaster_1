﻿using DataAccessLibrary.Models;
using Microsoft.Data.Sqlite;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLibrary.DataAccess_Offline
{
    public class Menu_Off
    {
        public static List<Menu> GetData(string dbpath)
        {
            List<Menu> entries = new List<Menu>();
            Menu cpr = null;
            try
            {
                using (SqliteConnection db =
                   new SqliteConnection($"Filename={dbpath}"))
                {
                    db.Open();
                    SqliteCommand selectCommand = new SqliteCommand
                        ("Select * from Menu", db);
                    SqliteDataReader query = selectCommand.ExecuteReader();
                    while (query.Read())
                    {
                        cpr = new Menu()
                        {
                            Id = query.GetInt64(query.GetOrdinal("Id")),
                            Title = query.GetString(query.GetOrdinal("Title")),
                            Description = query.GetString(query.GetOrdinal("Description")),
                            Icon = query.GetString(query.GetOrdinal("Icon")),
                            Status = query.GetInt64(query.GetOrdinal("Status")),
                            Link = query.GetString(query.GetOrdinal("Link")),
                            LinkType = query.GetInt64(query.GetOrdinal("LinkType")),
                            ParentId = query.GetInt64(query.GetOrdinal("ParentId"))
                        };
                        entries.Add(cpr);
                    }
                    db.Close();
                }
            }
            catch(Exception ex) { return entries = new List<Menu>(); }
            return entries;
        }
    }
}
