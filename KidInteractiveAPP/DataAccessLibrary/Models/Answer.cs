﻿using DataAccessLibrary.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace DataAccessLibrary.Models
{
    public partial class Answer
    {
        public long Id { get; set; }
        public string Content { get; set; }
        public long IsCorrect { get; set; }
        public long Status { get; set; }
        public long QuestionId { get; set; }

        public virtual Question Question { get; set; }
    }

    public class ConvertAnswer
    {
        public long Id { get; set; }
        public string Content { get; set; }
        public bool IsCorrect { get; set; }
        public long Status { get; set; }
        public long QuestionId { get; set; }
    }
    public class ConvertAnswerRemaster : INotifyPropertyChanged
    {
        private long _id;
        public long Id
        {
            get { return _id; }
            set { _id = value; OnPropertyChanged("Id"); }
        }
        private string _content;
        public string Content
        {
            get { return _content; }
            set { _content = value; OnPropertyChanged("Content"); }
        }
        private bool _isCorrect;
        public bool IsCorrect
        {
            get { return _isCorrect; }
            set { _isCorrect = value; OnPropertyChanged("IsCorrect"); }
        }
        private long _status;
        public long Status
        {
            get { return _status; }
            set { _status = value; OnPropertyChanged("Status"); }
        }
        private long _questionId;
        public long QuestionId
        {
            get { return _questionId; }
            set { _questionId = value; OnPropertyChanged("QuestionId"); }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
