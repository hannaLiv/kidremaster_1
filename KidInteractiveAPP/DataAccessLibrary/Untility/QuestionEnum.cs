﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLibrary.Untility
{
    public enum QuestionQTypeEnum
    {
        Funny = 1,
        Scary = 2,
        Science = 3
    }
    public enum QuestionStatus
    {
        Publish = 1,
        Archive = 2
    }

    public enum AnswerStatus
    {
        Publish = 1,
        Archive = 2
    }
}
