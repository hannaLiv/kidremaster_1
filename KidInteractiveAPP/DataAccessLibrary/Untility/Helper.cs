﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLibrary.Untility
{
    public class Helper
    {
        public static DateTime UnixTimeStampToDateTime(byte[] unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddMilliseconds(BitConverter.ToInt64(unixTimeStamp,0)).ToLocalTime();
            return dtDateTime;
        }
        public static byte[] DateTimeToUnixTimeStamp(DateTime date)
        {
            var result = ((DateTimeOffset)date).ToUnixTimeSeconds();
            return BitConverter.GetBytes(result);
        }
    }
}
