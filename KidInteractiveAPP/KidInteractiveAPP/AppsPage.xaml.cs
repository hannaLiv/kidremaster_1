﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Microsoft.UI.Xaml;
using Microsoft.UI.Xaml.Controls;
using Microsoft.UI.Xaml.Controls.Primitives;
using Microsoft.UI.Xaml.Data;
using Microsoft.UI.Xaml.Input;
using Microsoft.UI.Xaml.Media;
using Microsoft.UI.Xaml.Navigation;
using KidInteractiveAPP.Menu;
using KidInteractiveAPP.MultiChoice;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace KidInteractiveAPP
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class AppsPage : Page
    {
        public AppsPage()
        {
            this.InitializeComponent();
        }        
        private void go_menu_click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(MenuPage));
        }
        private void go_multiplechoice_click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(MultiChoicePage));
        }
        private void logout_click(object sender, RoutedEventArgs e)
        {            
            Windows.Storage.ApplicationData.Current.LocalSettings.Values.Remove("isloginsuccess");
            this.Frame.Navigate(typeof(LoginPage));
            this.Frame.BackStack.RemoveAt(this.Frame.BackStack.Count - 1);
        }
    }
}
