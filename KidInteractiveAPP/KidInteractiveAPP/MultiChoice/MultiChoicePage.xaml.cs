﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Microsoft.UI.Xaml;
using Microsoft.UI.Xaml.Controls;
using Microsoft.UI.Xaml.Controls.Primitives;
using Microsoft.UI.Xaml.Data;
using Microsoft.UI.Xaml.Input;
using Microsoft.UI.Xaml.Media;
using Microsoft.UI.Xaml.Navigation;
using KidInteractiveAPP.Menu;
using DataAccessLibrary.Models;
using System.Collections.ObjectModel;
using DataAccessLibrary;
using KidInteractiveAPP.Utilities;
using Windows.Foundation.Metadata;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace KidInteractiveAPP.MultiChoice
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MultiChoicePage : Page
    {
        public static MultiChoicePage Current;
        public static Frame RootFrame = null;
        private ObservableCollection<DataAccessLibrary.Models.MultiChoice> Multi = new ObservableCollection<DataAccessLibrary.Models.MultiChoice>();
        private List<DataAccessLibrary.Models.MultiChoice> Mul;
        public MultiChoicePage()
        {
            
            this.InitializeComponent();
            Mul = DataAccess.MultiChoice_GetData(CoreConfig.OnlineMode(), CoreConfig.DatabaseName(), CoreConfig.Url_WebApi());
            //foreach (var item in r)
            //    Multi.Add(item);
        }
        private void btnCreate_Click(object sender, RoutedEventArgs e)
        {
            //this.Frame.Navigate(typeof(MenuPage));
            //var button = (Button)sender;

            //if((string)button.Content == "Create")
            //{
            //    button.Content = "Su";
            //}
            //this.Frame.Navigate(typeof(Edit));
            this.Frame.Navigate(typeof(Edit));
        }

        private void Modify_Click(object sender, RoutedEventArgs e)
        {
            var id_string = ((Button)sender).Tag.ToString();
            this.Frame.Navigate(typeof(Edit), id_string);
        }

        private void Questions_Click(object sender, RoutedEventArgs e)
        {
            var id_string = ((Button)sender).Tag.ToString();
            this.Frame.Navigate(typeof(QuestionManager), id_string);
        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            var id_string = int.Parse(((Button)sender).Tag.ToString());
            if(id_string > 0)
            {
                var re = DataAccess.MultiChoice_DeleteData(CoreConfig.OnlineMode(), CoreConfig.DatabaseName(), CoreConfig.Url_WebApi(), id_string);
                Mul = DataAccess.MultiChoice_GetData(CoreConfig.OnlineMode(), CoreConfig.DatabaseName(), CoreConfig.Url_WebApi());
            }

        }
    }
}
