﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Microsoft.UI.Xaml;
using Microsoft.UI.Xaml.Controls;
using Microsoft.UI.Xaml.Controls.Primitives;
using Microsoft.UI.Xaml.Data;
using Microsoft.UI.Xaml.Input;
using Microsoft.UI.Xaml.Media;
using Microsoft.UI.Xaml.Navigation;
using DataAccessLibrary.Untility;
using System.Collections.ObjectModel;
using System.Text;
using Newtonsoft.Json;
using DataAccessLibrary;
using KidInteractiveAPP.Utilities;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace KidInteractiveAPP.MultiChoice
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Edit : Page
    {
        private ObservableCollection<MultiChoiceCombo> status = new ObservableCollection<MultiChoiceCombo>();
        private ObservableCollection<MultiChoiceCombo> qType = new ObservableCollection<MultiChoiceCombo>();

        public Edit()
        {
            //var stauts = Enum.GetValues(typeof(MultiChoiceEnumStatus)).Cast<MultiChoiceEnumStatus>();
            var sList = Enum.GetValues(typeof(MultiChoiceEnumStatus))
               .Cast<MultiChoiceEnumStatus>()
               .Select(t => new MultiChoiceCombo
               {
                   Number = ((int)t),
                   Name = t.ToString()
               });
            var qtList = Enum.GetValues(typeof(MultiChoiceQuestionType))
               .Cast<MultiChoiceQuestionType>()
               .Select(t => new MultiChoiceCombo
               {
                   Number = ((int)t),
                   Name = t.ToString()
               });
            foreach (var item in sList)
            {
                status.Add(item);
            }
            foreach(var item in qtList)
            {
                qType.Add(item);
            }
            this.InitializeComponent();
            titleHeader.Text = "Create MultiChoice";
            
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            var id_string = (string)e.Parameter;
            titleHeader.Text = "Edit for " + id_string;
            var id = 0;
            var a = int.TryParse(id_string, out id);
            if(id > 0)
            {
                var multi_affected = DataAccess.MultiChoice_GetById(CoreConfig.OnlineMode(), CoreConfig.DatabaseName(), CoreConfig.Url_WebApi(), id);
                if(multi_affected != null)
                {
                    Topic.Text = multi_affected.Topic;
                    Category.Text = multi_affected.Category;
                    StartDate.SelectedDate = Helper.UnixTimeStampToDateTime(multi_affected.StartDate);
                    EndDate.SelectedDate = Helper.UnixTimeStampToDateTime(multi_affected.EndDate);
                    StatusCombo.SelectedValuePath = multi_affected.Status.ToString();
                    Order.Text = BitConverter.ToString(multi_affected.Position);
                    QuestionType.SelectedValuePath = multi_affected.QuestionType.ToString();

                }
            }
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var affected_id = 0;
                var title = titleHeader.Text;
                if (!title.Equals("Edit for "))
                    affected_id = int.Parse(title.Replace("Edit for ", "").Trim());
                var topic = Topic.Text;
                var cate = Category.Text;
                var created = Helper.DateTimeToUnixTimeStamp(DateTime.Now);
                var start = Helper.DateTimeToUnixTimeStamp(StartDate.SelectedDate.Value.DateTime);
                var end = Helper.DateTimeToUnixTimeStamp(EndDate.SelectedDate.Value.DateTime);
                var sList = Enum.GetValues(typeof(MultiChoiceEnumStatus))
                   .Cast<MultiChoiceEnumStatus>()
                   .Select(t => new MultiChoiceCombo
                   {
                       Number = ((int)t),
                       Name = t.ToString()
                   });
                var qtList = Enum.GetValues(typeof(MultiChoiceQuestionType))
                   .Cast<MultiChoiceQuestionType>()
                   .Select(t => new MultiChoiceCombo
                   {
                       Number = ((int)t),
                       Name = t.ToString()
                   });
                var s = 0;
                var qt = 0;
                if (StatusCombo.SelectedItem != null)
                {
                    var container = (MultiChoiceCombo)StatusCombo.SelectedItem;
                    s = container.Number;
                }
                if (QuestionType.SelectedItem != null)
                {
                    var container = (MultiChoiceCombo)QuestionType.SelectedItem;
                    qt = container.Number;
                }
                //var s = status.Where(r => r.Name.Equals(r_s)).FirstOrDefault() != null ? status.Where(r => r.Name.Equals(r_s)).FirstOrDefault().Number : -1;
                var order = Order.Text;
                DataAccessLibrary.Models.MultiChoice multi = new DataAccessLibrary.Models.MultiChoice();
                multi.Id = affected_id;
                multi.Topic = topic;
                multi.Category = cate;
                multi.CreatedDate = created;
                multi.StartDate = start;
                multi.EndDate = end;
                multi.Status = s;
                multi.Position = Encoding.ASCII.GetBytes(order);
                multi.QuestionType = qt;
                try
                {
                    if (affected_id == 0)
                    {
                        DataAccess.MultiChoice_InsertData(CoreConfig.OnlineMode(), CoreConfig.DatabaseName(), CoreConfig.Url_WebApi(), multi);
                    }
                    if (affected_id > 0)
                    {
                        DataAccess.MultiChoice_UpdateData(CoreConfig.OnlineMode(), CoreConfig.DatabaseName(), CoreConfig.Url_WebApi(), multi);
                    }
                    this.Frame.Navigate(typeof(MultiChoicePage));
                    this.Frame.BackStack.RemoveAt(this.Frame.BackStack.Count - 1);
                }
                catch (Exception)
                {

                }

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex);
            }
            
        }
    }
}
