﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Microsoft.UI.Xaml;
using Microsoft.UI.Xaml.Controls;
using Microsoft.UI.Xaml.Controls.Primitives;
using Microsoft.UI.Xaml.Data;
using Microsoft.UI.Xaml.Input;
using Microsoft.UI.Xaml.Media;
using Microsoft.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace KidInteractiveAPP
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class GamesPage : Page
    {
        public GamesPage()
        {
            this.InitializeComponent();
        }
        private void go_funnygame_click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(FunnyGamePage));
        }
        private void go_multichoice_click(object sender, RoutedEventArgs e)
        {

        }
        private void go_categoriesgame_click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(MenuGamePage));            
        }
    }
}
