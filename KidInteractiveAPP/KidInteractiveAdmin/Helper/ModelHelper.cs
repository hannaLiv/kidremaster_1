﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KidInteractiveAdmin.Helper
{
    public static class ModelHelper
    {
        private static string _current_api = "http://206.189.82.108:9119/";
        public static int GetTongCauHoi(int multi_id)
        {
            if (multi_id > 0)
            {
                var lst_cau_hoi = DataAccessLibrary.DataAccess_Online.Question_On.GetListQuestionByMultiChoiceId(_current_api, multi_id);
                return lst_cau_hoi.Count;
            }
            return 0;
        }
        public static IEnumerable<T> SelectNestedChildren<T>
                (this IEnumerable<T> source, Func<T, IEnumerable<T>> selector)
        {
            foreach (T item in source)
            {
                yield return item;
                foreach (T subItem in SelectNestedChildren(selector(item), selector))
                {
                    yield return subItem;
                }
            }
        }

    }
}
