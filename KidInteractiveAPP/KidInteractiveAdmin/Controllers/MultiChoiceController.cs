﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using DataAccessLibrary.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Remotion.Linq.Parsing.ExpressionVisitors.Transformation.PredefinedTransformations;

namespace KidInteractiveAdmin.Controllers
{
    public class MultiChoiceController : Controller
    {
        private string api = "http://206.189.82.108:9119/";
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        //[Route("Detail/{id}")]
        public IActionResult GetDetail(int id)
        {
            MultiChoiceDetail result = new MultiChoiceDetail();
            if (id > 0)
            {
                var multi = DataAccessLibrary.DataAccess_Online.MultiChoice_On.GetListMultiChoiceById(api, id);
                if (multi != null)
                {
                    //MultiChoiceDetail result = new MultiChoiceDetail();
                    result.Multi = multi;
                    var questions = DataAccessLibrary.DataAccess_Online.Question_On.GetListQuestionByMultiChoiceId(api, id);
                    List<QuestionDetail> questionDetails = new List<QuestionDetail>();
                    foreach (var q in questions)
                    {
                        var l = new QuestionDetail();
                        l.Question = q;
                        var an = new List<Answer>();
                        var lst_ans = DataAccessLibrary.DataAccess_Online.Answer_On.GetListAnswerByQuestionId(api, (int)q.Id);
                        foreach (var ls in lst_ans.Skip(0).Take(4))
                        {
                            an.Add(ls);
                        }
                        l.Answers = an;
                        questionDetails.Add(l);

                    }
                    result.QuestionDetails = questionDetails;
                    return PartialView("~/Views/MultiChoice/GetDetail.cshtml", result);
                }
            }
            if(id <= 0)
            {
                result = null;
                return PartialView("~/Views/MultiChoice/GetDetail.cshtml", result);
            }
            return BadRequest();
        }
        public IActionResult SaveMultichoice(MultiChoiceDetail details)
        {
            //Save thang Multi truoc
            if(details != null)
            {
                var multi = details.Multi;
                if(multi != null)
                {
                    if (multi.Id == 0)
                    {
                        var a = DataAccessLibrary.DataAccess_Online.MultiChoice_On.InsertMultiChoice(api, multi);
                        //return Ok(a);
                        var result_multichoice = JsonConvert.DeserializeObject<MultiChoice>(a);
                        var lst_ans = new List<string>();
                        var out_id = result_multichoice.Id;

                        foreach (var item in details.QuestionDetails)
                        {

                            var question = item.Question;
                            question.QuestionGroupId = out_id;
                            var b = DataAccessLibrary.DataAccess_Online.Question_On.SaveQuestion(api, question);
                            var result_question = JsonConvert.DeserializeObject<Question>(b);
                            var out_question_id = result_question.Id;
                            foreach(var it in item.Answers)
                            {
                                it.QuestionId = out_question_id;
                                var c = DataAccessLibrary.DataAccess_Online.Answer_On.SaveAnswer(api, it);
                                lst_ans.Add(c);       
                            }
                            
                        }
                        return Ok(lst_ans);
                    }
                    if(multi.Id > 0)
                    {
                        var a = DataAccessLibrary.DataAccess_Online.MultiChoice_On.UpdateMultiChoice(api, multi);
                        //return Ok(a);
                        var result_multichoice = JsonConvert.DeserializeObject<MultiChoice>(a);
                        var lst_ans = new List<string>();
                        //var out_id = result_multichoice.Id;

                        foreach (var item in details.QuestionDetails)
                        {

                            var question = item.Question;
                            question.QuestionGroupId = multi.Id;
                            var b = DataAccessLibrary.DataAccess_Online.Question_On.SaveQuestion(api, question);
                            var result_question = JsonConvert.DeserializeObject<Question>(b);
                            var out_question_id = item.Question.Id;
                            foreach (var it in item.Answers)
                            {
                                it.QuestionId = out_question_id;
                                var c = DataAccessLibrary.DataAccess_Online.Answer_On.SaveAnswer(api, it);
                                lst_ans.Add(c);
                            }

                        }
                        return Ok(a);
                    }
                }
                
            }
            return BadRequest();
            
            //return Ok(details);
        }
        public IActionResult DeleteMultichoice(int id)
        {
            var r = DataAccessLibrary.DataAccess_Online.MultiChoice_On.DeleteMultiChoice(api, id);
            return Ok(r);
        }
        public IActionResult RefreshPage()
        {
            var model = DataAccessLibrary.DataAccess_Online.MultiChoice_On.GetListMultiChoice(api);
            return PartialView(model);
        }
    }

    public class MultiChoiceDetail
    {
        public MultiChoice Multi { get; set; }
        public List<QuestionDetail> QuestionDetails { get; set; }
    }
    public class QuestionDetail
    {
        public Question Question { get; set; }
        public List<Answer> Answers { get; set; }
    }
}
