﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using YetAnotherShoppingApp;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace YetAnotherShoppingApp.MainPage.Quiz
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class SelectQuiz : Page
    {
        private List<MultiChoice> GetChoices;
        private static int _index = 1;
        private static int _size = 6;
        public static string _api = "http://206.189.82.108:9119/";
        private PlayerInfomation info = new PlayerInfomation();
        public SelectQuiz()
        {
            this.InitializeComponent();
            GetChoices= MultiChoiceController.GetMultis(_api).Skip(_index - 1).Take(_size).ToList();
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {

            base.OnNavigatedTo(e);
            var name = (string)e.Parameter;
            info.Name = name;
        }

        private void btn_select_quiz_Click(object sender, RoutedEventArgs e)
        {
            var id = 0;
            var a = int.TryParse(((Button)sender).Tag.ToString(), out id);
            var choosen = GetChoices.Where(r => r.Id == id).FirstOrDefault();
            if(choosen != null)
            {
                if (id > 0)
                {
                    info.Multi_Id = id;
                    info.Multi_Topic = choosen.Topic;
                    info.Multi_Category = choosen.Category;
                this.Frame.Navigate(typeof(ReadyQuiz), info);
                }
            }
            
        }
    }
    public class PlayerInfomation
    {
        public string Name { get; set; }
        public int Multi_Id { get; set; }
        public string Multi_Topic { get; set; }
        public string Multi_Category { get; set; }
    }
}
