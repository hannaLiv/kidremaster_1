﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using YetAnotherShoppingApp.Models;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace YetAnotherShoppingApp.MainPage.Quiz
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    /// 
    public sealed partial class ReadyQuiz : Page
    {
        private PlayerInfomation info;
        private static MultiChoiceDetails details = new MultiChoiceDetails();//dung ko
        public static string _api = "http://206.189.82.108:9119/";
        public ReadyQuiz()
        {
            this.InitializeComponent();
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {

            base.OnNavigatedTo(e);
            info = (PlayerInfomation)e.Parameter;
            if(info != null)
            {
                details.Topic = info.Multi_Topic;
                details.Category = info.Multi_Category;
                details.QuestionWithAnswers = new List<QuestionWithAnswers>();//Ở đây sai => Nếu không khởi tạo nó trong Contructor nó sẽ bằng null 
                player_name.Text = info.Name;
                //Get quest in Multi
                if (info.Multi_Id > 0)
                {
                    var list_quest = MultiChoiceController.GetListQuestionByMultiChoiceId(_api,info.Multi_Id);
                    count_question.Text = list_quest.Count.ToString();
                    foreach (var item in list_quest)
                    {
                        List<Answer> ls = new List<Answer>();
                        ls = MultiChoiceController.GetListAnswerByQuestionId(_api, (int)item.Id);
                        QuestionWithAnswers qwa = new QuestionWithAnswers(item, ls);
                        //Xem lai cho nay dhs loi
                        details.QuestionWithAnswers.Add(qwa);
                    }
                }
                    
            }
            
        }

        private void btn_play_Click(object sender, RoutedEventArgs e)
        {
            //Try
            this.Frame.Navigate(typeof(QuestionDetail), details);
        }
    }
    public class MultiChoiceDetails
    {
        public string Topic { get; set; }
        public string Category { get; set; }
        public List<QuestionWithAnswers> QuestionWithAnswers { get; set; }
        //public MultiChoiceDetails()
        //{
        //    this.QuestionWithAnswers = new List<QuestionWithAnswers>();
        //}

        
    }
    public class QuestionWithAnswers
    {
        public QuestionWithAnswers(Question question, List<Answer> answers)
        {
            Question = question;
            Answers = answers;
        }

        public Question Question { get; set; }
        public List<Answer> Answers { get; set; }
    }
    
}
