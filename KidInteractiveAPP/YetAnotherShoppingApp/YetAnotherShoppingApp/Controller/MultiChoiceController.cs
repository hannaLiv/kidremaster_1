﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using YetAnotherShoppingApp;
using YetAnotherShoppingApp.Models;

namespace YetAnotherShoppingApp
{
    public class MultiChoiceController
    {
        
        public static List<MultiChoice> GetMultis(string url_webapi)
        {
            var result = new List<MultiChoice>();
            try
            {
                HttpClient client = new HttpClient();
                //var jsonObj = JsonConvert.SerializeObject(filter);
                //var content = new StringContent(jsonObj, System.Text.Encoding.UTF8, "application/json");
                //content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                var resp = client.GetAsync(url_webapi + "api/MultiChoices").Result;
                string r = resp.Content.ReadAsStringAsync().Result;
                if (!string.IsNullOrEmpty(r))
                    result = JsonConvert.DeserializeObject<List<MultiChoice>>(r);
            }
            catch (Exception ex) { return result = new List<MultiChoice>(); }
            return result;
        }
        public static List<Question> GetListQuestion(string url_webapi)
        {
            var result = new List<Question>();
            try
            {
                HttpClient client = new HttpClient();
                //var jsonObj = JsonConvert.SerializeObject(filter);
                //var content = new StringContent(jsonObj, System.Text.Encoding.UTF8, "application/json");
                //content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                var resp = client.GetAsync(url_webapi + "api/Questions").Result;
                string r = resp.Content.ReadAsStringAsync().Result;
                if (!string.IsNullOrEmpty(r))
                    result = JsonConvert.DeserializeObject<List<Question>>(r);
            }
            catch (Exception ex) { return result = new List<Question>(); }
            return result;
        }
        public static List<Answer> GetListAnswerByQuestionId(string url_webapi, int id)
        {
            var result = new List<Answer>();
            try
            {
                HttpClient client = new HttpClient();
                //var jsonObj = JsonConvert.SerializeObject(filter);
                //var content = new StringContent(jsonObj, System.Text.Encoding.UTF8, "application/json");
                //content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                var resp = client.GetAsync(url_webapi + "api/Answers/GetAnswersByQuestionId/" + id).Result;
                string r = resp.Content.ReadAsStringAsync().Result;
                if (!string.IsNullOrEmpty(r))
                    result = JsonConvert.DeserializeObject<List<Answer>>(r);
            }
            catch (Exception ex) { return result = new List<Answer>(); }
            return result;
        }
        public static List<Question> GetListQuestionByMultiChoiceId(string url_webapi, int id)
        {
            var result = new List<Question>();
            try
            {
                HttpClient client = new HttpClient();
                //var jsonObj = JsonConvert.SerializeObject(filter);
                //var content = new StringContent(jsonObj, System.Text.Encoding.UTF8, "application/json");
                //content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                var resp = client.GetAsync(url_webapi + "api/Questions/MultiChoice/" + id).Result;
                string r = resp.Content.ReadAsStringAsync().Result;
                if (!string.IsNullOrEmpty(r))
                    result = JsonConvert.DeserializeObject<List<Question>>(r);
            }
            catch (Exception ex) { return result = new List<Question>(); }
            return result;
        }
    }
}
