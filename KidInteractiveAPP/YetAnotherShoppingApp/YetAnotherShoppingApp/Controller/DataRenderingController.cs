﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YetAnotherShoppingApp.Controller
{
    public static class DataRenderingController
    {
        public static List<Menu> RenderQuantityMenu(int quantity)
        {
            var result = new List<Menu>();
            for(int i = 0; i < quantity; i++)
            {
                var r = new Menu();
                r.Title = "Menu số " + i;
                r.Description = "Mô tả menu số " + i;
                result.Add(r);
            }
            return result;
        }

    }
}
