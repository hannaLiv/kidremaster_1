﻿//using DataAccessLibrary.DataAccess_Online;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using YetAnotherShoppingApp;

namespace YetAnotherShoppingApp
{
    public class GamesController
    {
        private static string _api = "http://206.189.82.108:9119/";
        private static List<Menu> GetData(string url_webapi)
        {
            List<Menu> entries = new List<Menu>();
            try
            {
                HttpClient client = new HttpClient();
                var resp = client.GetAsync(url_webapi + "Menu/GetData").Result;
                string result = resp.Content.ReadAsStringAsync().Result;
                if (!string.IsNullOrEmpty(result))
                    entries = JsonConvert.DeserializeObject<List<Menu>>(result);
            }
            catch (Exception ex) { return entries = new List<Menu>(); }
            return entries;
        }
        public static List<Menu> GetGames()
        {
            var menu_games = GetData(_api).Where(r => r.LinkType == 4 && r.ParentId > 0).ToList();

            //var result = new List<GameInfo>();

            //result.Add(new GameInfo() { Name = "Sweet Candy", Url = "https://cdn.htmlgames.com/SweetCandy/" });
            //result.Add(new GameInfo() { Name = "Mouse And Cheese", Url = "https://cdn.htmlgames.com/MouseAndCheese/" });
            //result.Add(new GameInfo() { Name = "Reach 10", Url = "https://cdn.htmlgames.com/Reach10/" });
            //result.Add(new GameInfo() { Name = "Cats 1010", Url = "https://cdn.htmlgames.com/Cats1010/" });
            //result.Add(new GameInfo() { Name = "Space Trip", Url = "https://cdn.htmlgames.com/SpaceTrip/" });
            //result.Add(new GameInfo() { Name = "EggAge", Url = "https://cdn.htmlgames.com/EggAge/" });
            //result.Add(new GameInfo() { Name = "Sweet Candy", Url = "https://cdn.htmlgames.com/SweetCandy/" });
            //result.Add(new GameInfo() { Name = "Mouse And Cheese", Url = "https://cdn.htmlgames.com/MouseAndCheese/" });
            //result.Add(new GameInfo() { Name = "Reach 10", Url = "https://cdn.htmlgames.com/Reach10/" });
            //result.Add(new GameInfo() { Name = "Cats 1010", Url = "https://cdn.htmlgames.com/Cats1010/" });
            //result.Add(new GameInfo() { Name = "Space Trip", Url = "https://cdn.htmlgames.com/SpaceTrip/" });
            //result.Add(new GameInfo() { Name = "EggAge", Url = "https://cdn.htmlgames.com/EggAge/" });
            //result.Add(new GameInfo() { Name = "Sweet Candy", Url = "https://cdn.htmlgames.com/SweetCandy/" });
            //result.Add(new GameInfo() { Name = "Mouse And Cheese", Url = "https://cdn.htmlgames.com/MouseAndCheese/" });
            //result.Add(new GameInfo() { Name = "Reach 10", Url = "https://cdn.htmlgames.com/Reach10/" });
            //result.Add(new GameInfo() { Name = "Cats 1010", Url = "https://cdn.htmlgames.com/Cats1010/" });
            //result.Add(new GameInfo() { Name = "Space Trip", Url = "https://cdn.htmlgames.com/SpaceTrip/" });
            //result.Add(new GameInfo() { Name = "EggAge", Url = "https://cdn.htmlgames.com/EggAge/" });


            return menu_games;
        }
    }
}
