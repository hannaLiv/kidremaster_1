﻿using System;
using System.Collections.Generic;

namespace YetAnotherShoppingApp
{
    public partial class MultiChoice
    {
        public MultiChoice()
        {
            Question = new HashSet<Question>();
        }

        public long Id { get; set; }
        public string Topic { get; set; }
        public string Category { get; set; }
        public byte[] CreatedDate { get; set; }
        public byte[] StartDate { get; set; }
        public byte[] EndDate { get; set; }
        public long Status { get; set; }
        public byte[] Position { get; set; }
        public long QuestionType { get; set; }

        public virtual ICollection<Question> Question { get; set; }
    }
}
