﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YetAnotherShoppingApp
{
    public static class UIHelper
    {
        private static Random rng = new Random();

        public static void Shuffle<T>(this IList<T> list)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }
        public static string RenderHtml(string body)
        {
            if (!string.IsNullOrEmpty(body))
            {
                body = body.Replace("[", "<div>").Replace("]", "</div>");

            }
            return body;
        }
        public static string StringBuilderHtml(string body, string title)
        {
            var r = "<!DOCTYPE html><html lang=\"en\" xmlns=\"http://www.w3.org/1999/xhtml\"><head> <meta charset=\"utf-8\" /> <title></title></head><body> <div id=\"flipbook\"> <div class=\"hard\">{{book_title}}</div> <div class=\"hard\"></div> {{renderBody}} <div class=\"hard\"></div> <div class=\"hard\"></div> </div> <script src=\"turnjs4/lib/turn.min.js\"></script> <script src=\"turnjs4/lib/zoom.min.js\"></script> <script type=\"text/javascript\"> $(\"#flipbook\").turn({ width: 400, height: 300, autoCenter: true }); </script></body ></html >";
            //var r = File.ReadAllText(file);
            r = r.Replace("{{book_title}}", title).Replace("{{renderBody}}", body);
            //ReadHtmlFile
            return r;

        }
    }
}
