﻿//using DataAccessLibrary;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Universial.Controller;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Universial
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Multichoice_List : Page
    {

        private static int pageIndex = 1;
        private static int pageSize = 4;
        private static string api = "http://206.189.82.108:9119/";
        public Multichoice_List()
        {
            

            this.InitializeComponent();
            //Load ra du lieu Cau hoi
            pos_1.Visibility = Visibility.Collapsed;
            pos_2.Visibility = Visibility.Collapsed;
            pos_3.Visibility = Visibility.Collapsed;
            pos_4.Visibility = Visibility.Collapsed;
            var multichoices = MultiChoiceController.GetMultis(api).Skip((pageIndex - 1)*pageSize).Take(pageSize).ToList();
            if(multichoices.Count >= 1)
            {
                pos_1.Visibility = Visibility.Visible;
                var item = multichoices[0];
                pos_1.Content = item.Topic;
                pos_1.Tag = item.Id;

            }
            if (multichoices.Count >= 2)
            {
                pos_2.Visibility = Visibility.Visible;
                var item = multichoices[1];
                pos_2.Content = item.Topic;
                pos_2.Tag = item.Id;

                
            }
            if (multichoices.Count >= 3)
            {
                pos_3.Visibility = Visibility.Visible;
                var item = multichoices[2];
                pos_3.Content = item.Topic;
                pos_3.Tag = item.Id;
            }
            if (multichoices.Count >= 4)
            {
                pos_4.Visibility = Visibility.Visible;
                var item = multichoices[3];
                pos_4.Content = item.Topic;
                pos_4.Tag = item.Id;
            }
        }

        private void pos_1_Click(object sender, RoutedEventArgs e)
        {
            var id_string = ((Button)sender).Tag.ToString();
            this.Frame.Navigate(typeof(Multichoice_Play), id_string);
            
        }

        private void pos_2_Click(object sender, RoutedEventArgs e)
        {
            var id_string = ((Button)sender).Tag.ToString();
            this.Frame.Navigate(typeof(Multichoice_Play), id_string);
        }
        private void pos_3_Click(object sender, RoutedEventArgs e)
        {
            var id_string = ((Button)sender).Tag.ToString();
            this.Frame.Navigate(typeof(Multichoice_Play), id_string);
        }
        private void pos_4_Click(object sender, RoutedEventArgs e)
        {
            var id_string = ((Button)sender).Tag.ToString();
            this.Frame.Navigate(typeof(Multichoice_Play), id_string);
        }

        private void btn_back_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(MainPage), null);
        }
    }

}
