﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Universial.Models
{
    public class InputModel
    {
        public string Email { get; set; }        
        public string Password { get; set; }
        public bool RememberMe { get; set; }
    }
}
