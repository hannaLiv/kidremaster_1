﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Universial
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Games : Page
    {
        public Games()
        {
            
            this.InitializeComponent();
            var site = "https://play.famobi.com/sudoku-classic";
            web_display.Navigate(new Uri(site, UriKind.Absolute));
        }

        private void btn_back_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(MainPage), null);
        }
        private void btn_game1_Click(object sender, RoutedEventArgs e)
        {
            var site = "https://play.famobi.com/sudoku-classic";
            web_display.Navigate(new Uri(site, UriKind.Absolute));
        }
        private void btn_game2_Click(object sender, RoutedEventArgs e)
        {
            var site = "https://games.famobi.com/best-games/candy-bubble";
            web_display.Navigate(new Uri(site, UriKind.Absolute));
        }
        private void btn_game3_Click(object sender, RoutedEventArgs e)
        {
            var site = "https://play.famobi.com/onet-connect-classic";
            web_display.Navigate(new Uri(site, UriKind.Absolute));
        }
    }
}
