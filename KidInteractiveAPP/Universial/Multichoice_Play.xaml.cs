﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Universial.Controller;
using Universial.Models;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Universial
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Multichoice_Play : Page
    {
        private List<Question> Ques;
        private List<Question> Display;
        private static Random rng = new Random();
        private static int CurrentQues = 1;
        private static string api = "http://206.189.82.108:9119/";
        public Multichoice_Play()
        {
            this.InitializeComponent();
            hidden_Id.Visibility = Visibility.Collapsed;
            tbx_validate_1.Visibility = Visibility.Collapsed;
            tbx_validate_2.Visibility = Visibility.Collapsed;
            tbx_validate_3.Visibility = Visibility.Collapsed;
            tbx_validate_4.Visibility = Visibility.Collapsed;

        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {

            base.OnNavigatedTo(e);
            var id_string = (string)e.Parameter;
            var id = 0;
            var a = int.TryParse(id_string, out id);
            hidden_Id.Text = id_string;
            if (id > 0)
            {
                Display = MultiChoiceController.GetListQuestion(api);
                foreach (var item in Display)
                {
                    item.Answer = Controller.MultiChoiceController.GetListAnswerByQuestionId(api, (int)item.Id);
                }
                //Hien thi ra thang dau tien
                var first = Display.Skip(CurrentQues-1).Take(1).FirstOrDefault();
                DisplayFunction(first);
            }
        }

        private void DisplayFunction(Question question)
        {
            var first = question;
            if (first != null)
            {
                tbx_identity.Text = CurrentQues.ToString();
                questContent.Text = question.Content;
                if (first.Answer.Count >= 1)
                {
                    ans_1.Content = first.Answer.ToList()[0].Content;
                    ans_1.Tag = first.Answer.ToList()[0].IsCorrect;
                }
                if (first.Answer.Count >= 2)
                {
                    ans_2.Content = first.Answer.ToList()[1].Content;
                    ans_2.Tag = first.Answer.ToList()[1].IsCorrect;
                }
                if (first.Answer.Count >= 3)
                {
                    ans_3.Content = first.Answer.ToList()[2].Content;
                    ans_3.Tag = first.Answer.ToList()[2].IsCorrect;
                }
                if (first.Answer.Count >= 4)
                {
                    ans_4.Content = first.Answer.ToList()[3].Content;
                    ans_4.Tag = first.Answer.ToList()[3].IsCorrect;
                }
            }

        }
        private void NextPage()
        {
            CurrentQues = CurrentQues + 1;
            var first = Display.Skip(CurrentQues - 1).Take(1).FirstOrDefault();
            RefreshPage();
            DisplayFunction(first);
        }
        private void PrevPage()
        {
            CurrentQues = CurrentQues - 1;

            var first = Display.Skip(CurrentQues - 1).Take(1).FirstOrDefault();
            RefreshPage();
            DisplayFunction(first);
        }
        private void RefreshPage()
        {
            TextBlock[] textBlocks = new TextBlock[] { tbx_validate_1, tbx_validate_2, tbx_validate_3, tbx_validate_4 };
            foreach(var item in textBlocks)
            {
                item.Visibility = Visibility.Collapsed;
            }
            
            CheckBox[] checkboxes = new CheckBox[] { ans_1, ans_2,
                                             ans_3, ans_4 };
            foreach(var item in checkboxes)
            {
                item.IsChecked = false;
            }
        }
        public class DisplayQuestion
        {
            public Question Question { get; set; }
            public List<Answer> Answers { get; set; }
        }

        private void ans_1_Checked(object sender, RoutedEventArgs e)
        {
            var isChecked = int.Parse(((CheckBox)sender).Tag.ToString());
            if(isChecked > 0)
            {
                tbx_validate_1.Text = "(V) Dung";
                //NextPage();
                
            }
            else
            {
                tbx_validate_1.Text = "(X) Sai";

            }
            tbx_validate_1.Visibility = Visibility.Visible;

        }

        private void ans_2_Checked(object sender, RoutedEventArgs e)
        {
            var isChecked = int.Parse(((CheckBox)sender).Tag.ToString());
            if (isChecked > 0)
            {
                tbx_validate_2.Text = "(V) Dung";
                //NextPage();
            }
            else
            {
                tbx_validate_2.Text = "(X) Sai";
            }
            tbx_validate_2.Visibility = Visibility.Visible;
        }

        private void ans_3_Checked(object sender, RoutedEventArgs e)
        {
            var isChecked = int.Parse(((CheckBox)sender).Tag.ToString());
            if (isChecked > 0)
            {
                tbx_validate_3.Text = "(V) Dung";
                //NextPage();
            }
            else
            {
                tbx_validate_3.Text = "(X) Sai";
            }
            tbx_validate_3.Visibility = Visibility.Visible;
        }

        private void ans_4_Checked(object sender, RoutedEventArgs e)
        {
            var isChecked = int.Parse(((CheckBox)sender).Tag.ToString());
            if (isChecked > 0)
            {
                tbx_validate_4.Text = "(V) Dung";
                //NextPage();
            }
            else
            {
                tbx_validate_4.Text = "(X) Sai";
            }
            tbx_validate_4.Visibility = Visibility.Visible;
        }

        private void btn_prev_Click(object sender, RoutedEventArgs e)
        {
            if (CurrentQues > 1)
                PrevPage();
        }

        private void btn_next_Click(object sender, RoutedEventArgs e)
        {
            if (CurrentQues < Display.Count)
                NextPage();
        }

        private void btn_back_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(Multichoice_List), null);
        }
    }
}
