﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using KidInteractive.Models;
using KidInteractiveAPI.Models;
using Newtonsoft.Json.Linq;

namespace KidInteractive.Controllers
{
    public class HomeController : Controller
    {
        KidInteractiveContext _context = new KidInteractiveContext();
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public IActionResult DbSynchronized([FromBody] obj_Table lObj1)
        {
            SyncData_ToJson json =new SyncData_ToJson();                        
            return Ok(json.GetData(lObj1));
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }                
        
    }
}
