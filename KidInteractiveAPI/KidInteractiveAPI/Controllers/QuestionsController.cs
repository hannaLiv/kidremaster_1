﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using KidInteractiveAPI.Models;
using Microsoft.Extensions.Logging;
using KidInteractiveAPI.Untility;
using System.Runtime;

namespace KidInteractiveAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QuestionsController : ControllerBase
    {
        KidInteractiveContext _context = new KidInteractiveContext();

        #region ModifyCode
        [HttpPost]
        [Route("Filter")]
        public IActionResult Filter([FromBody] QuestionFilter filter)
        {
            var result = _context.Question.AsQueryable();
            if (filter != null)
            {

                if (!string.IsNullOrEmpty(filter.Title))
                    result = result.Where(r => r.Title.Contains(filter.Title)).AsQueryable();
                if (!string.IsNullOrEmpty(filter.Content))
                    result = result.Where(r => r.Content.Contains(filter.Content)).AsQueryable();
                if (filter.Status != null)
                    result = result.Where(r => r.Status == filter.Status.Value).AsQueryable();
                if (filter.QType != null)
                    result = result.Where(r => r.Qtype == filter.QType.Value).AsQueryable();
                if (filter.Point != null)
                    result = result.Where(r => r.Point == filter.Point.Value).AsQueryable();
                if (filter.QuestionGroupId != null)
                    result = result.Where(r => r.QuestionGroupId == filter.QuestionGroupId.Value).AsQueryable();
                //var index = filter.Index ?? 1;
                //var size = filter.Size ?? 20;
                //result = result.Skip(index).Take(size);
            }
            return Ok(result);
        }
        [Route("MultiChoice/{id}")]
        public IActionResult GetByMultiChoiceId(int id)
        {
            if(id > 0)
            {
                var result = _context.Question.Where(r => r.QuestionGroupId == id).ToList();
                return Ok(result);
            }
            return BadRequest();
        }


        #endregion

        // GET: api/Questions
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Question>>> GetQuestion()
        {
            return await _context.Question.Include("Answer").ToListAsync();
        }
        
        // GET: api/Questions/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Question>> GetQuestion(long id)
        {
            var question = await _context.Question.FindAsync(id);

            if (question == null)
            {
                return NotFound();
            }

            return question;
        }

        // PUT: api/Questions/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutQuestion(long id, Question question)
        {
            if (id != question.Id)
            {
                return BadRequest();
            }

            _context.Entry(question).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!QuestionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Questions
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Question>> PostQuestion(Question question)
        {
            _context.Question.Add(question);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetQuestion", new { id = question.Id }, question);
        }

        // DELETE: api/Questions/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Question>> DeleteQuestion(long id)
        {
            var question = await _context.Question.FindAsync(id);
            if (question == null)
            {
                return NotFound();
            }
            var list_answers = _context.Answer.Where(r => r.QuestionId == id);
            _context.Answer.RemoveRange(list_answers);
            _context.Question.Remove(question);
            await _context.SaveChangesAsync();

            return question;
        }

        private bool QuestionExists(long id)
        {
            return _context.Question.Any(e => e.Id == id);
        }
    }
}
