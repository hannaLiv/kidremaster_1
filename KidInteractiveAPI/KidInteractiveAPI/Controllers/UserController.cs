﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using KidInteractive.Models;
using KidInteractiveAPI.Models;
using Microsoft.AspNetCore.Identity;
using static KidInteractiveAPI.Areas.Identity.Pages.Account.LoginModel;

namespace KidInteractive.Controllers
{
    public class UserController : Controller
    {
        
        KidInteractiveContext _context = new KidInteractiveContext();
        private readonly ILogger<HomeController> _logger;
        private readonly SignInManager<IdentityUser> _signInManager;

        public UserController(SignInManager<IdentityUser> signInManager, ILogger<HomeController> logger)
        {
            _logger = logger;
            _signInManager = signInManager;
        }

        [HttpPost]
        public async Task<IActionResult> Login([FromBody] InputModel userinfo)
        {            
            var result = await _signInManager.PasswordSignInAsync(userinfo.Email, userinfo.Password, userinfo.RememberMe, lockoutOnFailure: false);     
            return Ok(result.Succeeded);
        }
    }
}
