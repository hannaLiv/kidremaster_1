﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KidInteractiveAPI.Untility
{
    public class Helper
    {
        public static DateTime UnixTimeStampToDateTime(byte[] unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddMilliseconds(BitConverter.ToDouble(unixTimeStamp)).ToLocalTime();
            return dtDateTime;
        }
    }
}
