﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KidInteractiveAPI.Untility
{
    public class MultiChoiceFilter
    {
        //string topic, string category, byte[] start_date, byte[] end_date, int? status, byte[] position, int? questionType, 
        //int? index, int? size, out int total
        public string Topic { get; set; }
        public string Category { get; set; }
        public byte[] StartDate { get; set; }
        public byte[] EndDate { get; set; }
        public int? Status { get; set; }
        public byte[] Position { get; set; }
        public int? QuestionType { get; set; }
        //public int? Index { get; set; }
        //public int? Size { get; set; }


    }
    public class QuestionFilter
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public int? Status { get; set; }
        public int? QType { get; set; }
        public int? Point { get; set; }
        public int? QuestionGroupId { get; set; }
        //public int? Index { get; set; }
        //public int? Size { get; set; }

    }
}
