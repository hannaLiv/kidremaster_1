﻿using System;
using System.Collections.Generic;

namespace KidInteractiveAPI.Models
{
    public partial class Copyright
    {
        public long Id { get; set; }
        public string CompanyName { get; set; }
        public long? YearOfCopy { get; set; }
        public string LicenseType { get; set; }
        public string Address { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
    }
}
