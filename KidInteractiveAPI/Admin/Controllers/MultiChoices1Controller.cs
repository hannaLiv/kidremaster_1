﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using KidInteractiveAPI.Models;

namespace Admin.Controllers
{
    public class MultiChoices1Controller : Controller
    {
        private readonly KidInteractiveContext _context;

        public MultiChoices1Controller(KidInteractiveContext context)
        {
            _context = context;
        }

        // GET: MultiChoices1
        public async Task<IActionResult> Index()
        {
            return View(await _context.MultiChoice.ToListAsync());
        }

        // GET: MultiChoices1/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var multiChoice = await _context.MultiChoice
                .FirstOrDefaultAsync(m => m.Id == id);
            if (multiChoice == null)
            {
                return NotFound();
            }

            return View(multiChoice);
        }

        // GET: MultiChoices1/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: MultiChoices1/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Topic,Category,CreatedDate,StartDate,EndDate,Status,Position,QuestionType")] MultiChoice multiChoice)
        {
            if (ModelState.IsValid)
            {
                _context.Add(multiChoice);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(multiChoice);
        }

        // GET: MultiChoices1/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var multiChoice = await _context.MultiChoice.FindAsync(id);
            if (multiChoice == null)
            {
                return NotFound();
            }
            return View(multiChoice);
        }

        // POST: MultiChoices1/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("Id,Topic,Category,CreatedDate,StartDate,EndDate,Status,Position,QuestionType")] MultiChoice multiChoice)
        {
            if (id != multiChoice.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(multiChoice);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MultiChoiceExists(multiChoice.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(multiChoice);
        }

        // GET: MultiChoices1/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var multiChoice = await _context.MultiChoice
                .FirstOrDefaultAsync(m => m.Id == id);
            if (multiChoice == null)
            {
                return NotFound();
            }

            return View(multiChoice);
        }

        // POST: MultiChoices1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            var multiChoice = await _context.MultiChoice.FindAsync(id);
            _context.MultiChoice.Remove(multiChoice);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool MultiChoiceExists(long id)
        {
            return _context.MultiChoice.Any(e => e.Id == id);
        }
    }
}
